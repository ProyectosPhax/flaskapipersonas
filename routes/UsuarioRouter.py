from flask import Flask,request
from flask_restful import Resource
from controllers import usuarioController

class Personas(Resource):
    def get(self):
        return usuarioController.getPersonas(request)
    
    def post(self):
        return usuarioController.agregarPersona(request)
    
    def put(self):
        return usuarioController.actualizarPersona(request)

    def delete(self,id):
        return usuarioController.borrarPersona(id)

