from flask import Flask,jsonify,request
from flask_cors import CORS, cross_origin
from app import app
from app import db
from app import ma
from app import cors
from models import Usuario
from datetime import date
import json

@cross_origin()
def getPersonas(request):
    
    rows = ["nombre", "fechanacimiento", "puesto"]
    personas = Usuario.Personas.query.all()
    result = Usuario.personas_schema.dump(personas)

    return jsonify(result)

@cross_origin()
def agregarPersona(request):
    
    nombre = request.json['nombre']
    fechanacimiento = request.json['fechanacimiento']
    puesto = request.json['puesto']

    nuevaPersona = Usuario.Personas(nombre,fechanacimiento,puesto)
    
    db.session.add(nuevaPersona)
    db.session.commit()

    return jsonify({"msg":"agregado"})

@cross_origin()
def actualizarPersona(request):

    id = request.json['id']

    persona = Usuario.Personas.query.get(id)

    if persona is None:
        return jsonify({"msg":"Persona no encontrada"})

    nombre = request.json['nombre']
    fechanacimiento = request.json['fechanacimiento']
    puesto = request.json['puesto']

    persona.nombre = nombre
    persona.fechanacimiento = fechanacimiento
    persona.puesto = puesto

    db.session.commit()
    
    return Usuario.persona_schema.jsonify(persona)

@cross_origin()
def borrarPersona(id):
    persona = Usuario.Personas.query.get(id)

    if persona is None:
        return jsonify({"msg":"Persona no encontrada"})

    db.session.delete(persona)
    db.session.commit()

    return Usuario.persona_schema.jsonify(persona)
