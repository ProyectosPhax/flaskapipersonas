from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from app import app
from app import db
from app import ma

class Personas(db.Model):

    __tablename__ = 'personas'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(20),unique=True,nullable=False)
    fechanacimiento = db.Column(db.DateTime,nullable=False)
    puesto = db.Column(db.String(20),nullable=False)

    def __init__(self,nombre,fechanacimiento,puesto):
        self.nombre = nombre
        self.fechanacimiento = fechanacimiento
        self.puesto = puesto

class PersonaSchema(ma.Schema):
    class Meta:
        fields = ('id','nombre','fechanacimiento','puesto')

personas_schema = PersonaSchema(many=True)
persona_schema = PersonaSchema()