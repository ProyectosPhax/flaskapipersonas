from flask import Flask
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS, cross_origin


app = Flask(__name__)
api = Api(app)
cors = CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://grwgzptguetthy:6c5338e4433ad404297d75cd55334957681603b9c0cdb142d1c717596cb283bc@ec2-107-22-163-220.compute-1.amazonaws.com/d3dudchofvr70c'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CORS_HEADERS'] = 'Content-Type'
db = SQLAlchemy(app)
ma = Marshmallow(app)
from routes.MainRouter import *




if __name__ == '__main__':
    app.run(debug=True, port=4000)